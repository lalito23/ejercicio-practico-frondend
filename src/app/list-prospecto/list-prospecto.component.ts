import { Component, OnInit } from '@angular/core';
import { ControlContainer, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { ProspectoApiService } from '../services/prospecto-api.service';

@Component({
  selector: 'app-list-prospecto',
  templateUrl: './list-prospecto.component.html',
  styleUrls: ['./list-prospecto.component.css']
})
export class ListProspectoComponent implements OnInit {

  // Global variables
  private form :FormGroup;
  private formDoc :FormGroup;

  public documentos = [];

  public archivo : any = null;

  public prospectos = [];

  public prospecto :any = null;

  public urlDocumento :string = "";

  public title : string = "";
  public autorizacion : boolean = false;

  // constructor
  constructor(private modalService: NgbModal,
    private formCenter: FormBuilder,
    private _prospectoApiService : ProspectoApiService) { }

  // funtion initial (load data)  
  ngOnInit() {
    this._prospectoApiService.getAll('Prospecto').subscribe((data: any) => {
      console.log('this.prospecto ',data);
      this.prospectos = data
    }, (err: any) => {
      Swal.fire({
        icon: 'error',
        text:'Error al cargar la información',
        showConfirmButton: true,
      })
    });
    this.archivo = null;
    this.form = this.formProspecto();
    this.formDoc = this.formDocumento();
  }

  // functions open modals
  openModal(targetModal){
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });
  }

  openModalDocumento(targetModal){
    this.archivo = null;
    this.formDoc = this.formDocumento();
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static'
    });
  }

  openModalDetail(targetModal, prospecto, autorizacion){
    this.prospecto = prospecto;
    this.autorizacion = autorizacion;
    this.title = autorizacion ? 'Autorizacion de prospecto' : 'Información prospecto'
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static'
    });
  }

  openModalVisualizar(targetModal, documento){
    this.urlDocumento = documento;
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static'
    });
  }

  // funtion init forms
  formProspecto() : FormGroup{
    return this.formCenter.group({
      nombre: ['', Validators.required],
      primer_ap: ['', Validators.required],
      segundo_ap: '',
      calle: ['', Validators.required],
      numero: ['', Validators.required],
      colonia: ['', Validators.required],
      cp: ['', Validators.required],
      telefono: ['', Validators.required],
      rfc: ['', Validators.required],
      documentos : null
    })
  }

  formDocumento() : FormGroup{
    return this.formCenter.group({
      nombre: ['', Validators.required]
    })
  }


  closeModal(){
    Swal.fire({
      title: '¿Estás seguro de salir de esta ventana?',
      text: 'Si cierras esta ventana la información se perdera',
      icon: 'question',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      /*confirmButtonColor: '#dc3f4e',*/
      confirmButtonText: 'Cerrar',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.archivo = null;
        this.form = this.formProspecto();
        this.formDoc = this.formDocumento();
        this.modalService.dismissAll();
      }
    })
  }

  // function petition http
  add(){
    Swal.fire({
      title: "Guardando....",
      imageUrl: "assets/images/loading.gif",
      showConfirmButton: false,
      allowOutsideClick: false
    });
    if (this.documentos.length == 0) {
      Swal.fire({
        icon: 'warning',
        text:'No se ha cargado ningun archivo',
        showConfirmButton: true,
      })
      return
    }
    let obj = {
      nombre: this.form.getRawValue().nombre,
      primer_ap: this.form.getRawValue().primer_ap,
      segundo_ap: this.form.getRawValue().segundo_ap,
      rfc: this.form.getRawValue().rfc,
      telefono: this.form.getRawValue().telefono,
      calle: this.form.getRawValue().calle,
      numero: this.form.getRawValue().numero,
      colonia: this.form.getRawValue().colonia,
      cp: this.form.getRawValue().cp,
      documentos : this.documentos
    }
    this.modalService.dismissAll();
    this._prospectoApiService.create('Prospecto', obj).subscribe((data: any) => {
      Swal.fire({
        icon: 'success',
        title: 'Se guardó correctamente.',
        showConfirmButton: true,
      })
      this.ngOnInit();
    }, (err: any) => {
      Swal.fire({
        icon: 'error',
        text:'Error al cargar la información',
        showConfirmButton: true,
      })
    });
  }

  autorizar(accion) {
    Swal.fire({
      title: '¿Estás seguro de '+accion+' al prospecto?',
      text: 'Una vez realizada esta acción no se prodra revertir el cambio.',
      icon: 'question',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Aceptar',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.modalService.dismissAll();
        if (accion == 'autorizar') {
          Swal.fire({
            title: "Autorizando....",
            imageUrl: "assets/images/loading.gif",
            showConfirmButton: false,
            allowOutsideClick: false
          });
          this.prospecto.estatus = 2;
          this._prospectoApiService.create('Prospecto/autorizar', this.prospecto).subscribe((data: any) => {
            Swal.fire({
              icon: 'success',
              title: 'Se autorizó el prospecto correctamente.',
              showConfirmButton: true,
            })
            this.ngOnInit();
          }, (err: any) => {
            Swal.fire({
              icon: 'error',
              text:'Error al cargar la información',
              showConfirmButton: true,
            })
          });
        }else{
          Swal.fire({
            text: 'Para continuar se debe agregar el motivo de rechazo.',
            input: 'text',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Continuar',
            showLoaderOnConfirm: true,
            reverseButtons: true
          }).then((result) => {
            if (result.isConfirmed) {
              result.value
              console.log('result.value', result.value);
              if (result.value != '') {
                Swal.fire({
                  title: "Rechazando....",
                  imageUrl: "assets/images/loading.gif",
                  showConfirmButton: false,
                  allowOutsideClick: false
                });
                this.prospecto.estatus = 3;
                this.prospecto.rechazo = {motivo:result.value, idProspecto: this.prospecto.id}
                this._prospectoApiService.create('Prospecto/autorizar', this.prospecto).subscribe((data: any) => {
                  Swal.fire({
                    icon: 'success',
                    title: 'Se rechazó el prospecto correctamente.',
                    showConfirmButton: true,
                  })
                  this.ngOnInit();
                }, (err: any) => {
                  Swal.fire({
                    icon: 'error',
                    text:'Error al cargar la información',
                    showConfirmButton: true,
                  })
                });
              }else{
                Swal.fire({
                  icon: 'error',
                  text:'Nos se pudo rechar el prospecto porque no se agrego el motivo de rechazo.',
                  showConfirmButton: true,
                })
              }
            }
          })
        }
      }
    })
  }

  //Function validate forms
  logValidationErrors(group: FormGroup) {
    Object.keys(group.controls).forEach((key: string) => {
      const ac = group.get(key);

      this.formsErrors[key] = '';
      if (ac && !ac.valid && (ac.touched || ac.dirty)) {
        const message = this.ValidationMessage[key];
        for (const errorKey in ac.errors) {
          if (errorKey) {
            this.formsErrors[key] += message[errorKey] + '    ';
          }
        }
      }
      if (ac instanceof FormGroup) {
        this.logValidationErrors(ac)
      }
    })
  }

  ValidationMessage = {
    nombre: { required: 'El nombre es necesario.' },
    primer_ap: { required: 'El primer ap es necesario.' },
    rfc: { required: 'El rfc es necesario.' },
    telefono: { required: 'El teléfono es necesario.' },
    calle: { required: 'La calle es necesaria.' },
    numero: { required: 'El número es necesario.' },
    colonia: { required: 'La colonia es necesaria.' },
    cp: { required: 'El C.P es necesario.' },
  }

  formsErrors = {
    nombre: '',
    primer_ap: '',
    rfc: '',
    telefono: '',
    calle: '',
    numero: '',
    colonia: '',
    cp: ''
  }
  //Fin------------------------------------------------ Funciones de validaciones del formulario

  // function load files
  onFileSelected() {
    const inputNode: any = document.querySelector('#file');
    if (typeof (FileReader) !== 'undefined') {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.archivo = reader.result as string;
      };
      reader.readAsDataURL(inputNode.files[0]);
    }
  }

  addDocumento(){
    if (this.archivo == null) {
      Swal.fire({
        icon: 'warning',
        title: 'No se ha seleccionado ningún documento.',
        showConfirmButton: true,
      })
      return
    }
    if (this.documentos.length == 0) {
      this.documentos.push({idx:0, nombre: this.formDoc.getRawValue().nombre, documento : this.archivo});
    }else{
      this.documentos.push({idx:this.documentos.length, nombre: this.formDoc.getRawValue().nombre, documento : this.archivo})
    }
    console.log('documentos', this.documentos);
    this.archivo = null;
    this.formDoc = this.formDocumento();
    
  }

  removeDocumento(documento){
    for(var i=0; i<this.documentos.length; i++){
      if(this.documentos[i].idx == documento.idx){
        this.documentos.splice(i,1);
          break;
      }
    }
    for(var i=0; i<this.documentos.length; i++){
      this.documentos[i].idx = i;
    }
  }
}
