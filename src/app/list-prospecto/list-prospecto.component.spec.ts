import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListProspectoComponent } from './list-prospecto.component';

describe('ListProspectoComponent', () => {
  let component: ListProspectoComponent;
  let fixture: ComponentFixture<ListProspectoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListProspectoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListProspectoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
