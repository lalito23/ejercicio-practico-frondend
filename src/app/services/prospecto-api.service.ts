import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProspectoApiService {

  baseURL = environment.apiURL;
  //baseURL = '/api/';
  headers = new HttpHeaders();

  constructor(
    private http: HttpClient, 
  ) {

  }

  // Create
  create<T>(model: T | any, objToCreate: T | any): Observable<T | T[]> {
    this.headers = new HttpHeaders();
    this.headers = this.headers.append('Authorization', 'bearer ');
    this.headers = this.headers.append('Content-Type', 'application/json');

    return this.http.post<T | T[]>(this.baseURL + model, objToCreate, {headers : this.headers});
  }

  getAll<T>(model: T | any): Observable<T[]> {
    this.headers = new HttpHeaders();
    this.headers = this.headers.append('Authorization', 'bearer ');
    this.headers = this.headers.append('Content-Type', 'application/json');

    console.log('holi');

    return this.http.get<T[]>(this.baseURL + model, {headers : this.headers});
  }
}
