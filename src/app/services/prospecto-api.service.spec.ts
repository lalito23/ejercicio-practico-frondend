import { TestBed } from '@angular/core/testing';

import { ProspectoApiService } from './prospecto-api.service';

describe('ProspectoApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProspectoApiService = TestBed.get(ProspectoApiService);
    expect(service).toBeTruthy();
  });
});
